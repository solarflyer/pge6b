PANDOC_PARS="markdown+escaped_line_breaks+superscript+subscript+implicit_figures+multiline_tables+pipe_tables+grid_tables+link_attributes+header_attributes+implicit_header_references"

FORWARDS=chapters/forward-trans.md  \
	 chapters/forward.md        

CHAPTERS=chapters/1.md              \
	 chapters/2.md		    \
         chapters/3.md

APPENDIXS=chapters/app-a.md	    

all:
	pandoc $(FORWARDS) -o preface.tex --toc  --from=$(PANDOC_PARS) --pdf-engine=xelatex -V documentclass=book 
	pandoc $(CHAPTERS) -o chapters.tex --toc --from=$(PANDOC_PARS) --pdf-engine=xelatex -V documentclass=book 
	pandoc $(APPENDIXS) -o appendix.tex --toc --from=$(PANDOC_PARS) --pdf-engine=xelatex -V documentclass=book
	pandoc --pdf-engine=xelatex chapters.tex -o e6b-zh.pdf --template template/template.tex 

clean:
	rm *.tex
	rm e6b-zh.pdf

